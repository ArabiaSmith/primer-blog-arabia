package com.blog;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BlogPrApplication {

	public static void main(String[] args) {
		SpringApplication.run(BlogPrApplication.class, args);
	}
}
