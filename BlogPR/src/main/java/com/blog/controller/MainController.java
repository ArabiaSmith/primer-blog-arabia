package com.blog.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import com.blog.dao.PreguntaDao;



@Controller
public class MainController {

	@Autowired
	private PreguntaDao preguntaDao;
	
	@GetMapping(value = "/")
	public String welcome(Model modelo) {
		
	// ESTAMOS LLAMANDO A LISTAPREGUNTA, PARA QUE SE VISUALICE DENTRO DE INDEX COMO BIEN DICE EN RETURN
		modelo.addAttribute("listaPregunta", preguntaDao.getAll());
		return "index";

	}
}