


	package com.blog.controller;

	import javax.servlet.http.HttpSession;

	import org.springframework.beans.factory.annotation.Autowired;
	import org.springframework.stereotype.Controller;
	import org.springframework.ui.Model;
	import org.springframework.web.bind.annotation.GetMapping;
	import org.springframework.web.bind.annotation.ModelAttribute;
	import org.springframework.web.bind.annotation.PathVariable;
	import org.springframework.web.bind.annotation.PostMapping;

import com.blog.beans.PreguntaBean;
import com.blog.beans.RespuestaBean;
import com.blog.dao.PreguntaDao;
import com.blog.dao.RespuestaDao;
import com.blog.model.Pregunta;
import com.blog.model.Profesor;
import com.blog.model.Respuesta;




	@Controller
	public class PreguntaController {

		@Autowired
		private PreguntaDao preguntaDao;
		
		@Autowired
		private RespuestaDao respuestaDao;
		
		@Autowired
		private HttpSession httpSession;

		@GetMapping(value = "/submit")
		public String showForm(Model modelo) {
			modelo.addAttribute("pregunta", new PreguntaBean());
			return "submit";
		}
		
		@PostMapping(value = "/submit/newpregunta")
		public String submit(@ModelAttribute("pregunta") PreguntaBean preguntaBean , Model model) {
			//userDao.create(new User(r.getNombre(), r.getEmail(), r.getCiudad(), r.getPassword()));
	      
			/*
			 * Tengo que crear una pregunta.
			 * Obtener el autor desde la sesión.
			 * Asignar el autor al post.
			 * Hacer persistente el post.
			*/
			
			
			// Todo lo que se escriba 'SET' por teclado (en la web) quiero que me lo devuelva 'GET'.
			Pregunta pregunta = new Pregunta();
			pregunta.setTitulo(preguntaBean.getTitulo());
			pregunta.setContenido(preguntaBean.getContenido());
		
			
			Profesor profesor = (Profesor) httpSession.getAttribute("userLoggedIn");
			pregunta.setProfesor(profesor);
			preguntaDao.create(pregunta);
			profesor.getPreguntas().add(pregunta);
			return "redirect:/";
		}	
		
		
		/* AQUÍ CREAMOS PODER ESCRIBIR LA RESPUESTA DENTRO DE LA PREGUNTA */
		@PostMapping(value = "/submit/newRespuesta")
		public String submitComment(@ModelAttribute("Respuesta") RespuestaBean respuestaBean , Model model) {
			//userDao.create(new User(r.getNombre(), r.getEmail(), r.getCiudad(), r.getPassword()));
			
			Profesor profesor = (Profesor) httpSession.getAttribute("userLoggedIn");
			
			Respuesta respuesta = new Respuesta();
			respuesta.setProfesor(profesor);
			
			Pregunta pregunta = preguntaDao.getById(respuestaBean.getPregunta_id());
			respuesta.setPregunta(pregunta);
			respuesta.setContenido(respuestaBean.getContenido());
			respuestaDao.create(respuesta);
			pregunta.getRespuestas().add(respuesta);
			profesor.getRespuestas().add(respuesta);
			
			return "redirect:/pregunta/" + respuestaBean.getPregunta_id();
		}	
		
		@GetMapping(value = "/pregunta/{id}")
		public String detail(@PathVariable("id")  long id, Model modelo) {
			//modelo.addAttribute("post", new PostBean());
			
			// Comprobar si el Post existe.
			
			Pregunta result = null;
			if ((result = preguntaDao.getById(id)) != null) {
				modelo.addAttribute("pregunta", result);
				modelo.addAttribute("respuestaForm", new RespuestaBean());
				return "preguntadetail";			
			} else
				return "redirect:/";
		}
	}



