package com.blog.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import com.blog.beans.RegisterBean;
import com.blog.dao.ProfesorDao;
import com.blog.model.Profesor;


@Controller
public class RegisterController {
	@Autowired
	private ProfesorDao profesorDao;

	@GetMapping(value = "/signup")
	public String showForm(Model model) {
		//Creo un nuevo RegisterBean llamado userRegister y me retorne "register"
		model.addAttribute("userRegister", new RegisterBean());
		return "register";
	}

	@PostMapping(value = "/register")
	//Cuando clico en userRegister, tiene que coger el modelo creado en RegisterBean
	//Y crear el profesor pasándole los atributos Nombre,email,etc...
	public String submit(@ModelAttribute("userRegister") RegisterBean r, Model model) {
		profesorDao.create(new Profesor(r.getNombre(), r.getEmail(), r.getCiudad(), r.getPassword()));
		
//     REVISAR ÉSTA REDIRECCIÓN!!!!
		return "redirect:/profesores";
	}
}
