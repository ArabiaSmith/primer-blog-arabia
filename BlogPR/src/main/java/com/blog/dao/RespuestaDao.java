package com.blog.dao;


import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;

import com.blog.model.Pregunta;
import com.blog.model.Respuesta;


@Repository
@Transactional
public class RespuestaDao {
	
    @PersistenceContext
	private EntityManager entityManager;
    /*
     * Almacena el post en la base de datos
     */
    public void create(Respuesta respuesta) {
    	entityManager.persist(respuesta);
    	return;
    }
    
    @SuppressWarnings("unchecked")
	public List<Respuesta> getAll(){
    	return entityManager
    			.createQuery("select c from Comment c")
    			.getResultList();
    }
    
    
 	/**
	 * Devuelve un post en base a su Id
	 */
	public Pregunta getById(long id) {
		return entityManager.find(Pregunta.class, id);
	}

    
    
    
}