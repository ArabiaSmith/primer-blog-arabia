package com.blog.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import org.hibernate.annotations.ColumnTransformer;

@Entity
public class Profesor {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private long id;
	
	@Column(name="nombre")
	private String nombre;
	
	@Column
	private String email;
	
	
	@Column
	private String ciudad;
	
	@Column
	@ColumnTransformer(write=" MD5(?) ")
	private String password;
	

	
	@OneToMany(mappedBy="profesor", fetch = FetchType.EAGER)	
	private Set<Respuesta> respuestas = new HashSet<>();
	
	
	public Set<Respuesta> getRespuestas() {
		return respuestas;
	}

	public void setRespuestas(Set<Respuesta> respuestas) {
		this.respuestas = respuestas;
	}

	public Set<Pregunta> getPreguntas() {
		return preguntas;
	}

	public void setPreguntas(Set<Pregunta> preguntas) {
		this.preguntas = preguntas;
	}

	@OneToMany(mappedBy="profesor" , fetch = FetchType.EAGER)	
	private Set<Pregunta> preguntas= new HashSet<>();	

	public Profesor() {
	}	
	
	public Profesor(String nombre, String email, String ciudad, String password) {
		
		this.nombre = nombre;
		this.email = email;
		this.ciudad = ciudad;
		this.password = password;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}


	public String getCiudad() {
		return ciudad;
	}

	public void setCiudad(String ciudad) {
		this.ciudad = ciudad;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	public String toString() {
		return "User [id=" + id + ", nombre=" + nombre + ", email=" + email + ", ciudad="
				+ ciudad + ", password=" + password + "]";
	}


	}

	
	
	