package com.blog.model;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;



@Entity
public class Respuesta {

	@Id
	@GeneratedValue
	private long id;
	
	@ManyToOne
	@JoinColumn(name="profesor_id", updatable=false)
	private Profesor profesor;

	
	@ManyToOne
	@JoinColumn(name="pregunta", updatable=false)
	private Pregunta pregunta;

	
	@Column
	@Lob
	private String contenido;


	public Respuesta(Profesor profesor, Pregunta pregunta, String contenido) {
		super();
		this.profesor = profesor;
		this.pregunta = pregunta;
		this.contenido = contenido;
	}


	public Respuesta() {
		super();
	}


	public long getId() {
		return id;
	}


	public void setId(long id) {
		this.id = id;
	}


	public Profesor getProfesor() {
		return profesor;
	}


	public void setProfesor(Profesor profesor) {
		this.profesor = profesor;
	}


	public Pregunta getPregunta() {
		return pregunta;
	}


	public void setPregunta(Pregunta pregunta) {
		this.pregunta = pregunta;
	}


	public String getContenido() {
		return contenido;
	}


	public void setContenido(String contenido) {
		this.contenido = contenido;
	}



	
	
}

